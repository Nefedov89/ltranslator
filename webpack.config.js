// Packages.
const webpack = require('webpack');
const path = require('path');

// Common constants.
const publicPath = path.resolve(__dirname, './public/assets/');
const resourcePath = path.resolve(__dirname, './resources/assets/');

// Webpack plugins.
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    context: resourcePath,
    entry: {
        // App js.
        'js/app.js': [
            './app/js/common.js'
        ],
        'js/translation.js': './app/js/translation.js',
        // Theme js.
        'js/theme.js': [
            './theme/js/core/libraries/jquery.min.js',
            './theme/js/core/libraries/bootstrap.min.js',
            './theme/js/plugins/ui/nicescroll.min.js',
            './theme/js/plugins/ui/drilldown.js',
            './theme/js/plugins/forms/selects/select2.min.js',
            './theme/js/plugins/forms/styling/uniform.min.js',
            './theme/js/core/app.js'
        ],
        // Theme css.
        'css/theme.css': './theme/less/index.less',
        'css/icons.css': './theme/css/icons/icomoon/styles.css',
    },
    output: {
        filename: '[name]',
        path: publicPath,
    },
    module: {
        rules: [
            {
                test: /\.(css|scss|less)$/,
                loader: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {url: false}
                        },
                        {
                            loader: 'sass-loader'
                        },
                        {
                            loader: 'less-loader'
                        },
                    ],
                }),
            },
        ]
    },
    plugins: [
        new ExtractTextPlugin('[name]'),
        new CopyWebpackPlugin([
            {
                from: `${resourcePath}/theme/images`,
                to: `${publicPath}/images`,
            },
            {
                from: `${resourcePath}/theme/css/icons/icomoon/fonts`,
                to: `${publicPath}/css/fonts`,
            }
        ]),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            'window.$': 'jquery',
            moment: 'moment'
        }),
    ]
};