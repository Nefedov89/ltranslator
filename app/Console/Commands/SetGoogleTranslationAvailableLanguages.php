<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SetGoogleTranslationAvailableLanguages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:update-google-languages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update list of available languages list for google translation api';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Start');

        $response = json_decode(
            file_get_contents(config('general.google_api_languages_url')),
            true
        );
        $updatedList = [];

        DB::table('google_supported_languages')->truncate();

        foreach ($response['data']['languages'] as $data) {
            $updatedList[] = ['language' => $data['language']];
        }

        if ($updatedList) {
            DB::table('google_supported_languages')->insert($updatedList);
        }

        $this->info('Finish');
    }
}
