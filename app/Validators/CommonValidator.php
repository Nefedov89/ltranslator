<?php

declare(strict_types = 1);

namespace App\Validators;

use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Validator;

/**
 * Class CommonValidator
 *
 * @package App\Validators
 */
class CommonValidator extends Validator
{
    /**
     * @param string $attribute
     * @param        $value
     * @param array  $parameters
     *
     * @return bool
     */
    public function validateIsPhpMimeType(
        string $attribute,
        $value,
        array $parameters
    ): bool {
        if ($value instanceof UploadedFile) {
            return $value->getMimeType() === 'text/x-php';
        }

        return false;
    }
}
