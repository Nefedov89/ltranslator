<?php

declare(strict_types = 1);

namespace App\Notifications;

use App\Models\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class EditorAddedToProject
 * @package App\Notifications
 */
class EditorAddedToProject extends Notification
{
    use Queueable;

    /**
     * @var Project
     */
    private $project;

    /**
     * Create a new notification instance.
     *
     * @param Project $project
     */
    public function __construct(Project $project)
    {
        //
        $this->project = $project;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line(
                trans(
                    'notification.editor_added_to_project.line_1',
                    ['project' => $this->project->name])
            )->action(
                trans('notification.editor_added_to_project.action'),
                route('translation.index').'?project='.$this->project->getKey()
            );
    }
}
