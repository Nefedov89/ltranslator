<?php

declare(strict_types = 1);

namespace App\Models;

use App\Models\Traits\DateHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\Media;

/**
 * Class Translation
 * @package App\Models
 */
class Translation extends Model implements HasMediaConversions
{
    use HasMediaTrait, DateHelper;

    /**
     * @var string
     */
    protected $table = 'translations';

    /**
     * @var array
     */
    protected $fillable = [
        'project_id',
        'source_lang',
        'target_lang',
        'completion_progress',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'project_id'            => 'number',
        'source_lang'           => 'string',
        'target_lang'           => 'string',
        'completion_progress'   => 'float',
    ];

    /**
     * @return BelongsTo
     */
    public function project(): BelongsTo
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * @return float
     */
    public function getCompletionPercentageAttribute(): float
    {
        return round($this->getAttribute('completion_progress') * 100);
    }

    /**
     * @return Media
     */
    public function getSourceFileAttribute(): Media
    {
        return $this->getFirstMedia('translation.source');
    }

    /**
     * @return Media
     */
    public function getTargetFileAttribute(): Media
    {
        return $this->getFirstMedia('translation.target');
    }

    /**
     * @param Media|null $media
     */
    public function registerMediaConversions(Media $media = null)
    {

    }
}
