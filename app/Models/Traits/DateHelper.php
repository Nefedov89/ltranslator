<?php

declare(strict_types = 1);

namespace App\Models\Traits;

use Carbon\Carbon;

/**
 * Class DateHelper
 * @package App\Entities\Traits
 */
trait DateHelper
{
    /**
     * Get created at attribute.
     *
     * @param $value
     *
     * @return $this|\DateTime
     */
    public function getCreatedAtAttribute($value)
    {
        return $this->formatWithTimeZone($value);
    }

    /**
     * Get updated at attribute.
     *
     * @param $value
     *
     * @return $this|\DateTime
     */
    public function getUpdatedAtAttribute($value)
    {
        return $this->formatWithTimeZone($value);
    }

    /**
     * Convert to time zone.
     *
     * @param $date
     *
     * @return string
     */
    private function formatWithTimeZone($date)
    {
        $date = (new Carbon($date))->setTimezone(config('app.displayed_timezone'));

        return $date;
    }
}
