<?php

declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Http\Requests\Translation\StoreRequest;
use App\Jobs\TranslateLangFile;
use App\Models\Project;
use App\Models\Translation;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;
use PeterColes\Languages\LanguagesFacade;
use Spatie\MediaLibrary\Exceptions\FileCannotBeAdded;
use Spatie\MediaLibrary\Exceptions\InvalidConversion;
use Spatie\MediaLibrary\Media;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Class TranslationController
 * @package App\Http\Controllers
 */
class TranslationController extends Controller
{
    /**
     * @param Request $request
     *
     * @return View
     */
    public function index(Request $request): View
    {
        $user = Auth::user();

        $translationsQuery = Translation::whereIn(
            'project_id', $user->projects()->pluck('id')
        )->latest();

        if ($projectId = $request->query('project')) {
            $translationsQuery = $translationsQuery->where('project_id', $projectId);
        }

        return view(
            'translation.index',
            [
                'projects'     => $user->projects,
                'translations' => $translationsQuery
                    ->paginate(static::ITEMS_PER_PAGE)
            ]
        );
    }

    /**
     * @return View
     *
     * @throws AuthorizationException
     */
    public function create(): View
    {
        $this->authorize('create', Translation::class);

        $availableLanguages = DB::table('google_supported_languages')
            ->pluck('language')
            ->toArray();
        $languages = LanguagesFacade::lookup($availableLanguages);

        return view(
            'translation.create',
            [
                'projects'  => Auth::user()->projects,
                'languages' => $languages
            ]
        );
    }

    /**
     * @param StoreRequest $request
     *
     * @return RedirectResponse
     *
     * @throws AuthorizationException
     *
     * @throws FileCannotBeAdded
     */
    public function store(StoreRequest $request): RedirectResponse
    {
        $this->authorize('create', Translation::class);

        /** @var Project $project */
        $project = Project::findOrFail($request->get('project_id'));

        $this->authorize('get', $project);

        /** @var Translation $translation */
        $translation = $project->translations()->create($request->all());

        // Translate.
        /** @var UploadedFile $sourceFile */
        $sourceFile = $request->file('file');
        $targetFilePath = sys_get_temp_dir().'/'.str_random(8);
        $copied = File::copy($sourceFile, $targetFilePath);

        $translation->addMedia($sourceFile)
            ->toMediaCollection('translation.source');

        if ($copied) {
            $targetTranslation = $translation
                ->addMediaFromUrl($targetFilePath)
                ->setName(
                    str_before(
                        $sourceFile->getClientOriginalName(),
                        '.'.$sourceFile->getClientOriginalExtension()
                    )
                )
                ->setFileName($sourceFile->getClientOriginalName())
                ->toMediaCollection('translation.target');
        }

        // Dispatch translation job.
        TranslateLangFile::dispatch($translation, $targetTranslation);

        return redirect(
            route(
                'translation.progress',
                [
                    'translation' => $translation->getKey(),
                ]
            )
        )->with('info', trans('flash_messages.translation.translating'));
    }

    /**
     * @param Translation $translation
     *
     * @return JsonResponse
     *
     * @throws AuthorizationException
     * @throws \Exception
     */
    public function destroy(Translation $translation): JsonResponse
    {
        $this->authorize('manipulate', $translation);

        $translation->clearMediaCollection('translation');
        $translation->delete();

        Session::flash('success', trans('flash_messages.translation.deleted'));

        return response()->json(['redirect' => route('translation.index')]);
    }

    /**
     * @param Translation $translation
     *
     * @return BinaryFileResponse
     *
     * @throws AuthorizationException
     */
    public function downloadTranslation(Translation $translation): BinaryFileResponse
    {
        $this->authorize('get', $translation);

        return response()->download($translation->target_file->getPath());
    }

    /**
     * @param Translation $translation
     *
     * @return View
     *
     * @throws AuthorizationException
     */
    public function editTranslationFile(Translation $translation): View
    {
        $this->authorize('get', $translation);

        $sourceTranslation = include $translation->source_file->getPath();
        $targetTranslation = include $translation->target_file->getPath();
        $sourceTranslationStrings = $targetTranslationStrings = [];

        array_walk_recursive(
            $sourceTranslation,
            function($value, $key) use (&$sourceTranslationStrings) {
                if (is_string($value)) {
                    $sourceTranslationStrings[] = $value;
                }
            }
        );

        array_walk_recursive(
            $targetTranslation,
            function($value, $key) use (&$targetTranslationStrings) {
                if (is_string($value)) {
                    $targetTranslationStrings[] = $value;
                }
            }
        );

        return view(
            'translation.edit',
            [
                'translation'              => $translation,
                'file'                     => $translation->target_file,
                'sourceTranslationStrings' => $sourceTranslationStrings,
                'targetTranslationStrings' => $targetTranslationStrings,
            ]
        );
    }

    /**
     * @param Request $request
     * @param Translation $translation
     *
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function updateTranslationFile(Request $request, Translation $translation): RedirectResponse
    {
        $this->authorize('get', $translation);

        $path = $translation->target_file->getPath();

        $fileContent = File::get($path);

        foreach ($request->get('strings') as $originalString =>  $updatedString) {
            $fileContent = str_replace($originalString, $updatedString, $fileContent);
        }

        File::put($path, $fileContent);

        $translation->updated_at = Carbon::now();
        $translation->save();

        return redirect(
            route(
                'translation.edit.file',
                ['translation' => $translation]
            ))
            ->with(
                'info',
                trans('flash_messages.translation.file.updated')
            );
    }

    /**
     * @param Translation $translation
     *
     * @return View
     *
     * @throws AuthorizationException
     */
    public function progress(Translation $translation): View
    {
        $this->authorize('manipulate', $translation);

        return view('translation.progress', ['translation' => $translation]);
    }

    /**
     * @param Translation $translation
     *
     * @return JsonResponse
     */
    public function completionPercentage(Translation $translation): JsonResponse
    {
        return response()->json(['percentage' => $translation->completion_percentage]);
    }
}