<?php

declare(strict_types = 1);

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class GeneralController
 * @package App\Http\Controllers
 */
class GeneralController extends Controller
{
    /**
     * Home.
     *
     * @return View
     */
    public function home(): View
    {
        return view('home');
    }
}