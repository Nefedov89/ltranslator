<?php

declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Http\Requests\Project\AddEditorRequest;
use App\Http\Requests\Project\StoreRequest;
use App\Http\Requests\Project\UpdateRequest;
use App\Models\Project;
use App\Models\User;
use App\Notifications\AccountCreated;
use App\Notifications\EditorAddedToProject;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

/**
 * Class ProjectController
 * @package App\Http\Controllers
 */
class ProjectController extends Controller
{
    /**
     * @param Request $request
     *
     * @return View
     */
    public function index(Request $request): View
    {
        $projectsQuery = Auth::user()->projects();

        if ($name = $request->query('name')) {
            $projectsQuery = $projectsQuery->where('name', 'like', "%{$name}%");
        }

        return view(
            'project.index',
            [
                'projects' => $projectsQuery
                    ->paginate(static::ITEMS_PER_PAGE)
            ]
        );
    }

    /**
     * @return View
     *
     * @throws AuthorizationException
     */
    public function create(): View
    {
        $this->authorize('create', Project::class);

        return view('project.create');
    }

    /**
     * @param StoreRequest $request
     *
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(StoreRequest $request): RedirectResponse
    {
        $this->authorize('create', Project::class);

        Auth::user()->projects()->create($request->all());

        return redirect(route('project.index'))
            ->with('success', trans('flash_messages.project.created'));
    }

    /**
     * @param Project $project
     *
     * @return View
     *
     * @throws AuthorizationException
     */
    public function edit(Project $project): View
    {
        $this->authorize('manipulate', $project);

        return view('project.edit', ['project' => $project]);
    }

    /**
     * @param UpdateRequest $request
     * @param Project $project
     *
     * @return RedirectResponse
     *
     * @throws AuthorizationException
     */
    public function update(UpdateRequest $request, Project $project): RedirectResponse
    {
        $this->authorize('manipulate', $project);

        $project->update($request->all());

        return redirect(route('project.index'))
            ->with('success', trans('flash_messages.project.updated'));
    }

    /**
     * @param Project $project
     *
     * @return JsonResponse
     *
     * @throws AuthorizationException
     * @throws \Exception
     */
    public function destroy(Project $project): JsonResponse
    {
        $this->authorize('manipulate', $project);

        $project->delete();

        Session::flash('success', trans('flash_messages.project.deleted'));

        return new JsonResponse(['redirect' => route('project.index')]);
    }

    /**
     * @param Project $project
     *
     * @return View
     * @throws AuthorizationException
     */
    public function getAddEditorPage(Project $project): View
    {
        $this->authorize('manipulate', $project);

        return view('project.add_editor', ['project' => $project]);
    }

    /**
     * @param AddEditorRequest $request
     * @param Project $project
     *
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function addEditor(AddEditorRequest $request, Project $project): RedirectResponse
    {
        $this->authorize('manipulate', $project);

        $flashMsg = trans('flash_messages.project.editor_added');

        /** @var User $user */
        $user = User::where('email', $request->get('email'))->first();

        // Create new user;
        if (!$user) {
            $emailParts = explode('@', $request->get('email'));
            $password = str_random(8);

            /** @var User $user */
            $user = User::create([
                'name'     => reset($emailParts),
                'email'    => $request->get('email'),
                'password' => $password,
            ]);

            $user->notify(new AccountCreated($password));

            $user->assignRole('editor');
        }

        if (!$project->users->contains($user)) {
            $project->users()->attach($user->getKey());
        } else {
            $flashMsg = trans('flash_messages.project.editor_already_exists');
        }

        $user->notify(new EditorAddedToProject($project));

        return redirect(route('project.index'))
            ->with('success', $flashMsg);
    }
}