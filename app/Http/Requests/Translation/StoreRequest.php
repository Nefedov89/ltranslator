<?php

declare(strict_types = 1);

namespace App\Http\Requests\Translation;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreRequest
 * @package App\Http\Requests\Project
 */
class StoreRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'project_id'  => 'required|numeric|exists:projects,id',
            'source_lang' => 'required|string|different:target_lang',
            'target_lang' => 'required|string|different:source_lang',
            'file'        => 'required|file|is_php_mime_type',
        ];
    }
}