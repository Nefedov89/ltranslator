<?php

declare(strict_types = 1);

namespace App\Providers;

use App\Validators\CommonValidator;
use Illuminate\Support\Facades\Validator;

/**
 * Class ValidationServiceProvider
 *
 * @package App\Providers
 */
class ValidationServiceProvider extends \Illuminate\Validation\ValidationServiceProvider
{
    /**
     * @return void
     */
    public function boot(): void
    {
        Validator::extend('is_php_mime_type', 'CommonValidator@isPhpMimeType');

        Validator::resolver(function ($translator, $data, $rules, $messages) {
            return new CommonValidator($translator, $data, $rules, $messages);
        });
    }
}
