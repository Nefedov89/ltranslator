<?php

namespace App\Policies;

use App\Models\Translation;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class TranslationPolicy
 * @package App\Policies
 */
class TranslationPolicy
{
    use HandlesAuthorization;

    /**
     * @param User        $user
     * @param Translation $translation
     *
     * @return bool
     */
    public function get(User $user, Translation $translation): bool
    {
        return $translation->project->users->contains($user);
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function create(User $user): bool
    {
        return $user->hasRole('developer');
    }

    /**
     * @param User        $user
     * @param Translation $translation
     *
     * @return bool
     */
    public function manipulate(User $user, Translation $translation): bool
    {
        return $user->hasRole('developer') && $this->get($user, $translation);
    }
}
