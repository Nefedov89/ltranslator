<?php

namespace App\Policies;

use App\Models\Project;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class ProjectPolicy
 * @package App\Policies
 */
class ProjectPolicy
{
    use HandlesAuthorization;

    /**
     * @param User    $user
     * @param Project $project
     *
     * @return bool
     */
    public function get(User $user, Project $project): bool
    {
        return $project->users->contains($user);
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function create(User $user): bool
    {
        return $user->hasRole('developer');
    }

    /**
     * @param User    $user
     * @param Project $project
     *
     * @return bool
     */
    public function manipulate(User $user, Project $project): bool
    {
        return $user->hasRole('developer') && $this->get($user, $project);
    }
}
