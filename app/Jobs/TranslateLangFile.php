<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Models\Translation;
use Exception;
use Google\Cloud\Translate\V2\TranslateClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Spatie\MediaLibrary\Exceptions\InvalidConversion;
use Spatie\MediaLibrary\Media;

/**
 * Class TranslateLangFile
 * @package App\Jobs
 */
class TranslateLangFile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Translation
     */
    private $translation;

    /**
     * @var Media
     */
    private $file;

    /**
     * @var bool|string
     */
    private $fileContent;

    /**
     * Create a new job instance.
     *
     * @param Translation $translation
     * @param Media $file
     *
     * @throws InvalidConversion
     */
    public function __construct(Translation $translation, Media $file)
    {
        $this->translation = $translation;
        $this->file = $file;
        $this->fileContent = File::get($file->getPath());
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws InvalidConversion
     * @throws \InvalidArgumentException
     */
    public function handle()
    {
        $translator = new TranslateClient([
            'key' => Config::get('google-translate.api_key'),
            'restOptions' => [
                'headers' => [
                    'referer' => URL::to('/')
                ]
            ]
        ]);
        $originalWordsDotted = array_dot($this->getTranslationArray());
        $wordsCount = count($originalWordsDotted);
        $iteration = 0;

        foreach ($originalWordsDotted as $key => $word) {
            $iteration++;

            try {
                $translation = $translator->translate(
                    $this->handleNoTranslationWords($word),
                    [
                        'source' => $this->translation->source_lang,
                        'target' => $this->translation->target_lang,
                    ]
                );

                if (ctype_upper($translation['input'][0])) {
                    $translation['text'] = ucfirst($translation['text']);
                }

                if (is_string($translation['text'])) {
                    $this->fileContent = preg_replace(
                        "/{$word}/",
                        $this->handleNoTranslationWords($translation['text'], false),
                        $this->fileContent
                    );
                }

                // Update completions percentage.
                $completionProgress = $iteration < $wordsCount
                    ? $this->translation->completion_progress + round((1 / $wordsCount), 2)
                    : 1;
                $this->translation->update(['completion_progress' => $completionProgress]);
            } catch (Exception $e) {
                Log::error($e->getMessage());
            }
        }

        // Update file content.
        File::put($this->file->getPath(), $this->fileContent);
    }

    /**
     * @return array
     *
     * @throws InvalidConversion
     */
    private function getTranslationArray(): array
    {
        return include $this->file->getPath();
    }

    /**
     * @param string $string
     * @param bool $up
     *
     * @return string
     */
    private function handleNoTranslationWords(string $string, $up = true): string
    {
        preg_match_all('/:[a-zA-Z0-9]+/', $string, $matches);

        foreach ($matches[0] as $match) {
            $wrappedWord = '<span translate="no">' . $match . '</span>';

            $string = $up === true
                ? str_replace($match, $wrappedWord, $string)
                : str_replace($wrappedWord, $match, $string);
        }

        return $string;
    }
}
