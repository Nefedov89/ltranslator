<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth.
// Authentication Routes.
$this->get('login', 'Auth\LoginController@showLoginForm')
    ->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')
    ->name('logout');

// Password Reset Routes.
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')
    ->name('password.request');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')
    ->name('password.email');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')
    ->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');

Route::group(['middleware' => 'auth'], function () {
    // Logout
    Route::get('logout', 'Auth\LoginController@logout')
        ->name('.logout');

    // Home.
    Route::get('/', 'GeneralController@home')
        ->name('home');

    // Projects.
    Route::resource('project', 'ProjectController');

    // Add editor.
    Route::get('project/{project}/add-editor', 'ProjectController@getAddEditorPage')
        ->middleware('role:developer')
        ->name('project.add-editor.get');

    Route::post('project/{project}/add-editor', 'ProjectController@addEditor')
        ->middleware('role:developer')
        ->name('project.add-editor.post');

    // Translations.
    Route::resource('translation', 'TranslationController');

    // Translation file edit.
    Route::get('translation/{translation}/edit', [
        'as'         => 'translation.edit.file',
        'uses'       => 'TranslationController@editTranslationFile',
    ]);

    // Translation file edit.
    Route::patch('translation/{translation}/edit', [
        'as'         => 'translation.update.file',
        'uses'       => 'TranslationController@updateTranslationFile',
    ]);

    // Translation file download.
    Route::get('translation/{translation}/download', 'TranslationController@downloadTranslation')
        ->name('translation.download');

    // Translation progress.
    Route::get('translation/{translation}/progress', 'TranslationController@progress')
        ->name('translation.progress');

    // Translation progress.
    Route::get('translation/{translation}/completion-percentage', 'TranslationController@completionPercentage')
        ->name('translation.completion-percentage');
});