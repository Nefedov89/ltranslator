<?php

declare(strict_types = 1);

use App\Models\User;
use Illuminate\Database\Seeder;

/**
 * Class UsersSeeder
 */
class UsersSeeder extends Seeder
{
    /**
     * @return void
     * @throws RuntimeException
     */
    public function run(): void
    {
        /** @var User $user */
        $user = User::create([
            'name'     => 'Amgrade',
            'email'    => 'amgrade@dev.tech',
            'password' => 'qwerty123',
        ]);

        $user->assignRole('developer');
    }
}
