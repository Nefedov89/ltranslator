#!/bin/bash

branch=$(git rev-parse --abbrev-ref HEAD)
remote=$(git remote)

git pull "$remote" "$branch"

changedFiles="$(git diff-tree -r --name-only --no-commit-id ORIG_HEAD HEAD)"

checkRun() {
    echo "$changedFiles" | grep --quiet "$1" && eval "$2"
}

composer dumpautoload
php artisan down

checkRun "composer.lock" "composer install -o"
checkRun "package.lock" "yarn"
checkRun "resources/assets" "yarn run build"

php artisan cache:clear
php artisan config:clear
php artisan view:clear
php artisan route:clear
php artisan clear-compiled
php artisan optimize
php artisan queue:restart
php artisan up
