require('./bootstrap');

$(document).ready(() => {
    // Progress bar.
    let $progressBar = $('#transProgressBar'),
        tid = $progressBar.data('tid'),
        initialPercentage = $progressBar.data('completion-percentage');

    if (initialPercentage !== 1) {
        let interval = setInterval(function() {
            $.ajax({
                type: 'GET',
                url: laroute.route('translation.completion-percentage', {'translation': tid}),
                error(data) {
                    console.error(data);
                },
                success(data) {
                    let percentage = data.percentage + '%';

                    $progressBar.css('width', percentage);
                    $('.completion-percentage-text').text(percentage);

                    if (data.percentage === 100) {
                        clearInterval(interval);

                        window.location.href = laroute.route('translation.index');
                    }
                }
            });
        }, 1000);
    }
});