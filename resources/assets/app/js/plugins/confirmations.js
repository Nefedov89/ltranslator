import swal from 'sweetalert';

const confirmation = {
    delete(success) {
        swal({
            title: Lang.get('confirmations.default.title'),
            text: Lang.get('confirmations.delete.text'),
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#FF5722",
            confirmButtonText: Lang.get('confirmations.delete.confirm'),
            cancelButtonText: Lang.get('common.cancel'),
        }, success);
    },

    getSwal() {
        return swal;
    }
};

window.confirmation = confirmation;