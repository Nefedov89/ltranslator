require('./bootstrap');

$(document).ready(() => {
    // Confirmation.
    $('.confirmation-delete').click(function (e) {
        const $this = $(this);

        e.preventDefault();

        confirmation.delete(() => {
            const url = $this.attr('href') || $this.data('url');

            $.ajax({
                method: 'DELETE',
                url,
                success(data) {
                    if ('redirect' in data) {
                        window.location.href = data.redirect;
                    } else if ('reload' in data) {
                        window.location.reload(true);
                    } else {
                        notify.success(Lang.trans('common.messages.success'), Lang.trans('common.word.success'));
                    }
                },
                error(errors) {
                    console.error(errors);
                }
            });
        });
    });
});