<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login'    => [
        'login_to_your_account'  => 'Login to your account',
        'enter_your_credentials' => 'Enter your credentials',
        'email'                  => 'Email',
        'password'               => 'Password',
        'forgot_password'        => 'Forgot password',
        'login'                  => 'Login',
        'dont_have_account'      => 'Don\'t have an account?',
        'sign_up'                => 'Sign up',
    ],
    'register' => [
        'create_account'          => 'Create account',
        'all_fields_are_required' => 'All fields are required',
        'your_credentials'        => 'Your credentials',
        'your_name'               => 'Your name',
        'your_email'              => 'Your email',
        'password'                => 'Password',
        'repeat_password'         => 'Repeat password',
        'register'                => 'Register',
    ],
    'password' => [
        'email' => [
            'password_recovery'  => 'Password recovery',
            'email_instructions' => 'We\'ll send you instructions in email',
            'your_email'         => 'Your email',
            'reset_password'     => 'Your email',
        ],
        'reset' => [
            'reset_password'   => 'Reset Password',
            'email'            => 'Email',
            'password'         => 'Password',
            'confirm_password' => 'Confirm Password',
        ],
    ],
];
