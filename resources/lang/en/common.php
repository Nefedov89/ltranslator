<?php

return [
    'logout'           => 'Logout',
    'my_profile'       => 'My Profile',
    'submit'           => 'Submit',
    'search'           => 'Search',
    'clear'            => 'Clear',
    'choose_file'      => 'Choose file',
    'no_file_selected' => 'No file selected',
    'create'           => 'Create',
    'update'           => 'Update',
    'delete'           => 'Delete',
    'cancel'           => 'Cancel',
    'edit'             => 'Edit',
    'download'         => 'Download',
    'add_editor'       => 'Add editor',
    'access_denied'    => 'Access denied',
    'or'               => 'or',
    'sidebar'          => [
        'menu'         => 'Menu',
        'home'         => 'Home',
        'projects'     => 'Projects',
        'translations' => 'Translation',
    ],
];
