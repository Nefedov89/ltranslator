<?php

return [
    'index'    => [
        'title'                   => 'Translations',
        'no_translations'         => 'You don\'t have any translations yet',
        'create'                  => 'Create translation',
        'update_google_languages' => 'Update google languages',
        'choose_project'          => 'Choose project',
        'table'                   => [
            'headers' => [
                'project_name'    => 'Project name',
                'files_count'     => 'Files count',
                'files'           => 'Files',
                'progress'        => 'Progress',
                'source_language' => 'Source language',
                'target_language' => 'Target language',
                'created'         => 'Created',
                'updated'         => 'Updated',
                'actions'         => 'Actions',
            ],
        ],
    ],
    'create'   => [
        'title'  => 'Create translation',
        'labels' => [
            'project'     => 'Project',
            'source_lang' => 'Source language',
            'target_lang' => 'Target language',
            'file'        => 'Your php lang file',
            'file_tips'   => 'Your php file must not contains any code except return array of key value (use cases, foreign class constants, etc)',
            'options'     => [
                'project'     => 'Choose project',
                'source_lang' => 'Choose source language',
                'target_lang' => 'Choose target language',
            ],
        ],
    ],
    'edit'     => [
        'title'         => 'Edit translation <strong class="label label-flat border-grey text-grey-600">:file</strong> for project <strong class="label label-flat border-grey text-grey-600">:project</strong>',
        'download'      => 'Download file',
        'params_alert'  => 'You must not edit specific words with a preceding colon like <strong>:param</strong>',
        'params_string' => 'You must not edit next words for this string: <strong>:params</strong>',
    ],
    'progress' => [
        'title' => 'Translation files progress for project <strong>:project</strong>',
    ],
];