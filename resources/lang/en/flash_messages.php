<?php

return [
    'project'     => [
        'created'               => 'A new project has been successfully created!',
        'updated'               => 'A new project has been successfully updated!',
        'editor_added'          => 'A new project editor has been successfully added!',
        'editor_already_exists' => 'This user is already editor of this project',
        'deleted'               => 'Project has been successfully deleted!',
    ],
    'translation' => [
        'created'             => 'A new translation has been successfully created!',
        'updated'             => 'A new translation has been successfully updated!',
        'deleted'             => 'Translation has been successfully deleted!',
        'google_lang_updated' => 'Google languages has been successfully updated',
        'translating'         => 'Wait for it..We are translating your file(s)...',
        'file'                => [
            'updated' => 'Translation file has been successfully updated',
        ],
    ],
];