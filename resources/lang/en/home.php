<?php

return [
    'about'       => 'LTranslator is simple helper for translation <a target="_blank" href="//laravel.com"><u>Laravel</u></a> project lang php files',
    'description' => [
        'all_you_need_is'   => 'All that you need is just:',
        'steps'             => [
            1 => 'Create Project',
            2 => 'Upload Laravel translation *.php files',
            3 => 'Choose desired language',
            4 => 'Wait for it...',
            5 => 'Download translated *.php files and put them into your project\'s lang directory',
        ],
        'edit_translations' => 'Also you can edit all translated strings and download edited file',
        'i_want_to_try'     => 'Cool! I want to try'
    ]
];
