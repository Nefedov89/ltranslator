<?php

declare(strict_types = 1);

return [
    'default' => [
        'title' => 'Are you sure?',
    ],

    'delete' => [
        'text'    => 'You will not be able to recover this',
        'confirm' => 'Yes, delete it!',
    ],

    'block' => [
        'confirm' => 'Yes, block!',
    ],

    'unblock' => [
        'confirm' => 'Yes, unblock!',
    ],

    'close_activity' => [
        'text'    => 'This action cannot be undo',
        'confirm' => 'Yes, close the activity',
    ],
];
