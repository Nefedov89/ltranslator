<?php

return [
    'index'      => [
        'title'              => 'Projects',
        'no_projects'        => 'You don\'t have any projects yet',
        'create'             => 'Create project',
        'translations'       => 'translations',
        'create_translation' => 'Create translation',
        'table'              => [
            'headers' => [
                'name'                => 'Project name',
                'translations_count'  => 'Translations count',
                'last_translation_at' => 'Last translation',
                'created'             => 'Created',
                'translations'        => 'Translations',
                'members'             => 'Members',
                'create_translation'  => 'Create translation',
                'actions'             => 'Actions',
            ],
        ],
    ],
    'create'     => [
        'title'  => 'Create project',
        'labels' => [
            'name' => 'Project name',
        ],
    ],
    'add_editor' => [
        'title'  => 'Add editor',
        'labels' => [
            'new'      => 'Create new user',
            'existing' => 'Add an existing user',
            'email'    => 'Email of new editor',
        ],
    ],
];