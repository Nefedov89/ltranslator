<?php

return [
    'account_created' => [
        'line_1' => 'Hi! We created account for you on LTranslator',
        'line_2' => 'Your password is: :password',
        'line_3' => 'Please visit it to edit your translations',
        'action' => 'Go to site',
    ],
    'editor_added_to_project' => [
        'line_1' => 'Hi! We added you to project :project',
        'action' => 'Go to project\'s translations',
    ],
];
