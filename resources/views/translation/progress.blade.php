@extends('layouts.app')

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">
                @lang('translation.progress.title', ['project' => $translation->project->name])
            </h5>
            <div class="heading-elements">
                <a href='{{ route('translation.index') }}' class="btn btn-success">
                    <i class="icon icon-list"></i>
                    @lang('translation.index.title')
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="content-group-sm">
                <p class="text-semibold">{{ $translation->target_file->file_name }}</p>
                <div class="progress">
                    <div id="transProgressBar" class="progress-bar bg-teal" data-tid="{{ $translation->id }}" data-completion-percentage="{{ $translation->completion_percentage }}" style="width: {{ $translation->completion_percentage }}%">
                        <span class="completion-percentage-text">{{ $translation->completion_percentage }}%</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_scripts')
    <script src="{{ asset('assets/js/translation.js') }}"></script>
@endsection