@extends('layouts.app')

@section('content')
    <div class="panel panel-flat col-md-9">
        <div class="panel-heading">
            <h5 class="panel-title">
                @lang('translation.create.title')
            </h5>
            <div class="heading-elements">
                <a href='{{ route('translation.index') }}' class="btn btn-success">
                    <i class="icon icon-list"></i>
                    @lang('translation.index.title')
                </a>
            </div>
        </div>
        <div class="panel-body">
            <form action="{{ route('translation.store') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('project_id') ? ' has-error' : '' }}">
                    <label for="project_id">@lang('translation.create.labels.project')</label>
                    <select name="project_id" class="select2 form-control">
                        <option disabled selected>@lang('translation.create.labels.options.project')</option>
                        @foreach($projects as $project)
                            <option @if(Request::query('project') == $project->id || old('project_id') == $project->id) selected @endif value="{{ $project->id }}">{{ $project->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('project_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('project_id') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('source_lang') ? ' has-error' : '' }}">
                    <label for="source_lang">@lang('translation.create.labels.source_lang')</label>
                    <select name="source_lang" class="select2 form-control">
                        <option disabled selected>@lang('translation.create.labels.options.source_lang')</option>
                        @foreach($languages as $key => $name)
                            <option @if(old('source_lang') === $key) selected @endif value="{{ $key }}">{{ $name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('source_lang'))
                        <span class="help-block">
                            <strong>{{ $errors->first('source_lang') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('target_lang') ? ' has-error' : '' }}">
                    <label for="target_lang">@lang('translation.create.labels.target_lang')</label>
                    <select name="target_lang" class="select2 form-control">
                        <option disabled selected>@lang('translation.create.labels.options.target_lang')</option>
                        @foreach($languages as $key => $name)
                            <option @if(old('target_lang') === $key) selected @endif value="{{ $key }}">{{ $name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('target_lang'))
                        <span class="help-block">
                            <strong>{{ $errors->first('target_lang') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                    <label for="files">@lang('translation.create.labels.file')</label>
                    <input type="file" class="file-styled form-control" name="file" multiple>

                    @if($errors->has('file'))
                        <span class="help-block">
                            <strong>{{ $errors->first('file') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="p-t-10 alert alert-danger no-border">
                    @lang('translation.create.labels.file_tips')
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-info">
                        @lang('common.submit')
                        <i class="icon-arrow-right14 position-right"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection