@extends('layouts.app')

@section('content')
    <div class="panel panel-flat col-md-9">
        <div class="panel-heading">
            <h5 class="panel-title">
                @lang('translation.edit.title', ['file' => $file->file_name, 'project' => $translation->project->name])
            </h5>
            <div class="heading-elements">
                <a href='{{ route('translation.index') }}' class="btn btn-success">
                    <i class="icon icon-list"></i>
                    @lang('translation.index.title')
                </a>
                <a href='{{ route('translation.download', ['translation' => $translation, 'media' => $file]) }}' class="btn btn-info">
                    <i class="icon icon-file-download2"></i>
                    @lang('translation.edit.download')
                </a>
            </div>
        </div>
        <div class="panel-body">
            <div class="alert alert-danger no-border">
                @lang('translation.edit.params_alert')
            </div>
            <form action="{{ route('translation.update.file', ['translation' => $translation, 'media' => $file]) }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PATCH">
                @foreach($targetTranslationStrings as $index => $string)
                    <div class="form-group">
                        @php
                            preg_match_all('/:[a-zA-Z0-9]+/', $sourceTranslationStrings[$index], $matches);
                            $label = $sourceTranslationStrings[$index];

                            foreach($matches[0] as $match) {
                                $label = str_replace($match, '<span class="label label-default">'.$match.'</span>', $label);
                            }
                        @endphp
                        <label for="">{!! $label !!}</label>
                        <input type="text" class="form-control" value="{{ $string }}" name="strings[{{ $string }}]" multiple>
                        @if($matches[0])
                            <label for="" class="help-block">
                                @lang('translation.edit.params_string', ['params' => implode(', ', $matches[0])])
                            </label>
                        @endif
                    </div>
                @endforeach
                <div class="text-right">
                    <button type="submit" class="btn btn-info">
                        @lang('common.submit')
                        <i class="icon-arrow-right14 position-right"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection