@extends('layouts.app')

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">
                @lang('translation.index.title')
            </h5>
            @role('developer')
                <div class="heading-elements">
                    <a href='{{ route('translation.create') }}@if(Request::query('project'))?project={{ Request::query('project') }}@endif' class="btn btn-success">
                        <i class="icon icon-plus2"></i>
                        @lang('translation.index.create')
                    </a>
                </div>
            @endrole
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                @if($translations->count())
                    <form action="{{ route('translation.index') }}">
                        <div class="form-group col-sm-3" style="padding-left: 0">
                            <div class="input-group">
                                <select name="project" class="select2 form-control">
                                    <option disabled selected>@lang('translation.index.choose_project')</option>
                                    @foreach($projects as $project)
                                        <option @if(Request::query('project') == $project->id) selected @endif value="{{ $project->id }}">{{ $project->name }}</option>
                                    @endforeach
                                </select>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">@lang('common.search')</button>
                                    <a href='{{ route('project.index') }}' class="btn btn-info">@lang('common.clear')</a>
                                </span>
                            </div>
                        </div>
                    </form>
                    <table class="table">
                        <thead>
                        <tr class="bg-info">
                            <th>#</th>
                            <th>@lang('translation.index.table.headers.project_name')</th>
                            <th>@lang('translation.index.table.headers.files')</th>
                            <th>@lang('translation.index.table.headers.progress')</th>
                            <th>@lang('translation.index.table.headers.source_language')</th>
                            <th>@lang('translation.index.table.headers.target_language')</th>
                            <th>@lang('translation.index.table.headers.created')</th>
                            <th>@lang('translation.index.table.headers.updated')</th>
                            @role('developer')
                                <th>@lang('translation.index.table.headers.actions')</th>
                            @endrole
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($translations as $translation)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    <a href="{{ route('project.edit', ['project' => $translation->project->id]) }}">
                                        <strong>{{ $translation->project->name }}</strong>
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('translation.edit.file', ['translation' => $translation->id]) }}" data-popup="tooltip" title="" data-original-title="@lang('common.edit')">
                                        <i class="icon icon-pencil7"></i>
                                    </a>
                                    <a href="{{ route('translation.download', ['translation' => $translation->id]) }}" data-popup="tooltip" title="" data-original-title="@lang('common.download')">
                                        <i class="icon  icon-file-download2"></i>
                                    </a>
                                    {{ $translation->target_file->file_name }}
                                    <br>
                                </td>
                                <td>
                                    <span class="label label-success">{{ $translation->completion_percentage }}%</span>
                                </td>
                                <td>
                                    <span class="label label-primary">
                                        {{ $translation->source_lang }}
                                    </span>
                                </td>
                                <td>
                                    <span class="label label-warning">
                                        {{ $translation->target_lang }}
                                    </span>
                                </td>
                                <td>{{ $translation->created_at->format('d/m/Y H:i') }}</td>
                                <td>{{ $translation->updated_at->format('d/m/Y H:i') }}</td>
                                @role('developer')
                                    <td>
                                        <ul class="icons-list">
                                            <li class="dropdown dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                                    <i class="icon-menu9"></i>
                                                </a>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li>
                                                        <a href="{{ route('translation.destroy', ['id' => $translation->id]) }}" class="confirmation-delete">
                                                            <i class="icon-trash"></i>
                                                            @lang('common.delete')
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                @endrole
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <h4>@lang('translation.index.no_translations')</h4>
                @endif
            </div>
        </div>
    </div>
    <div class="pagination text-center">{{ $translations->links() }}</div>
@endsection