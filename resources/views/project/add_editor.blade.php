@extends('layouts.app')

@section('content')
    <div class="panel panel-flat col-md-9">
        <div class="panel-heading">
            <h5 class="panel-title">
                @lang('project.add_editor.title')
            </h5>
            <div class="heading-elements">
                <a href='{{ route('project.index') }}' class="btn btn-success">
                    <i class="icon icon-list"></i>
                    @lang('project.index.title')
                </a>
            </div>
        </div>
        <div class="panel-body">
            <form action="{{ route('project.add-editor.post', ['project' => $project]) }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="@lang('project.add_editor.labels.new')"></label>
                    <input type="email" class="form-control" name="email" placeholder="@lang('project.add_editor.labels.email')">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-info">
                        @lang('common.add_editor')
                        <i class="icon-arrow-right14 position-right"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection