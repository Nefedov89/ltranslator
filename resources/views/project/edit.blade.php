@extends('layouts.app')

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">
                @lang('project.create.title')
            </h5>
            <div class="heading-elements">
                <a href='{{ route('project.index') }}' class="btn btn-success">
                    <i class="icon icon-list"></i>
                    @lang('project.index.title')
                </a>
            </div>
        </div>
        <div class="panel-body">
            <form action="{{ route('project.update', ['project' => $project->id]) }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PATCH">
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="title">@lang('project.create.labels.name')</label>
                    <input type="text" class="form-control" name="name" value="{{ $project->name }}">
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-info">
                        @lang('common.submit')
                        <i class="icon-arrow-right14 position-right"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection