@extends('layouts.app')

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">
                @lang('project.index.title')
            </h5>
            @role('developer')
                <div class="heading-elements">
                    <a href='{{ route('project.create') }}' class="btn btn-success">
                        <i class="icon icon-plus2"></i>
                        @lang('project.index.create')
                    </a>
                </div>
            @endrole
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                @if($projects->count())
                    <form action="{{ route('project.index') }}">
                        <div class="form-group col-sm-3" style="padding-left: 0">
                            <div class="input-group">
                                <input type="text" name="name" class="form-control" placeholder="@lang('project.index.table.headers.name')" value="{{ Request::query('name') ?? '' }}">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">@lang('common.search')</button>
                                    <a href='{{ route('project.index') }}' class="btn btn-info">@lang('common.clear')</a>
                                </span>
                            </div>
                        </div>
                    </form>
                    <table class="table">
                        <thead>
                        <tr class="bg-info">
                            <th>#</th>
                            <th>@lang('project.index.table.headers.name')</th>
                            <th>@lang('project.index.table.headers.translations_count')</th>
                            <th>@lang('project.index.table.headers.last_translation_at')</th>
                            <th>@lang('project.index.table.headers.created')</th>
                            <th>@lang('project.index.table.headers.translations')</th>
                            @role('developer')
                                <th>@lang('project.index.table.headers.members')</th>
                                <th>@lang('project.index.table.headers.create_translation')</th>
                                <th>@lang('project.index.table.headers.actions')</th>
                            @endrole
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($projects as $project)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    @role('developer')
                                        <a href="{{ route('project.edit', ['project' => $project->id]) }}">
                                            <strong>{{ $project->name }}</strong>
                                        </a>
                                    @endrole
                                    @role('editor')
                                        <strong>{{ $project->name }}</strong>
                                    @endrole
                                </td>
                                <td>{{ $project->translations->count() }}</td>
                                <td>
                                    @if($project->translations->count())
                                        {{ $project->translations->last()->created_at->format('d/m/Y H:i') }}
                                    @else
                                        ---
                                    @endif
                                </td>
                                <td>{{ $project->created_at->format('d/m/Y H:i') }}</td>
                                <td>
                                    <a href="{{ route('translation.index') }}?project={{ $project->id }}" class="btn border-slate text-teal-800 btn-flat">
                                        @lang('project.index.translations')
                                    </a>
                                </td>
                                @role('developer')
                                <td>
                                    @php $members = implode(', ', $project->users_names); @endphp
                                    <span data-popup="tooltip" title="" data-original-title="{{ $members }}">
                                        {{ str_limit($members, 10) }}
                                    </span>
                                </td>
                                <td>
                                    <a href="{{ route('translation.create') }}?project={{ $project->id }}" class="btn border-slate text-teal-800 btn-flat">
                                        @lang('project.index.create_translation')
                                    </a>
                                </td>
                                <td>
                                    <ul class="icons-list">
                                        <li class="dropdown dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                                <i class="icon-menu9"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li>
                                                    <a href="{{ route('project.add-editor.get', ['id' => $project->id]) }}">
                                                        <i class="icon icon-user-plus"></i>
                                                        @lang('common.add_editor')
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ route('project.edit', ['id' => $project->id]) }}">
                                                        <i class="icon-pencil7"></i>
                                                        @lang('common.edit')
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ route('project.destroy', ['id' => $project->id]) }}" class="confirmation-delete">
                                                        <i class="icon-trash"></i>
                                                        @lang('common.delete')
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                                @endrole
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <h4>@lang('project.index.no_projects')</h4>
                @endif
            </div>
        </div>
    </div>
    <div class="pagination text-center">{{ $projects->links() }}</div>
@endsection