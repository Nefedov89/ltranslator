@extends('layouts.app')

@section('content')
    <div class="panel panel-flat border-top-lg border-top-info">
        <div class="panel-heading">
            <h2 class="panel-title">@lang('home.about')</h2>
        </div>
        <div class="panel-body">
            <h3>@lang('home.description.all_you_need_is')</h3>
            <div class="steps alert alert-info no-border p-15">
                <span class="text-size-large">
                    <strong>
                        @lang('home.description.steps.1')
                    </strong>
                    <i class="icon icon-arrow-right13"></i>
                </span>
                <span class="text-size-large">
                    <strong>
                        @lang('home.description.steps.2')
                    </strong>
                    <i class="icon icon-arrow-right13"></i>
                </span>
                <span class="text-size-large">
                    <strong>
                        @lang('home.description.steps.3')
                    </strong>
                    <i class="icon icon-arrow-right13"></i>
                </span>
                <span class="text-size-large">
                    <strong>
                        @lang('home.description.steps.4')
                    </strong>
                    <i class="icon icon-arrow-right13"></i>
                </span>
                <span class="text-size-large">
                    <strong>
                        @lang('home.description.steps.5')
                    </strong>
                </span>
            </div>
            <h5>@lang('home.description.edit_translations')</h5>
            @role('developer')
                <div class="text-center">
                    <a href="{{ route('translation.create') }}" class="btn btn-info btn-xlg">
                        @lang('home.description.i_want_to_try')
                    </a>
                </div>
            @endrole
        </div>
    </div>
@endsection