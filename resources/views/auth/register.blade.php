@extends('layouts.guest')

@section('content')
    <!-- Page container -->
    <div class="page-container login-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Advanced login -->
                <form action="{{ route('register') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="panel panel-body login-form">
                        <div class="text-center">
                            <div class="icon-object border-success text-success"><i class="icon-plus3"></i></div>
                            <h5 class="content-group">
                                @lang('auth.register.create_account')
                                <small class="display-block">@lang('auth.register.all_fields_are_required')</small>
                            </h5>
                        </div>

                        <div class="content-divider text-muted form-group"><span>@lang('auth.register.your_credentials')</span></div>

                        <div class="form-group has-feedback has-feedback-left{{ $errors->has('name') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" placeholder="@lang('auth.register.your_name')" name="name" value="{{ old('name') }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                            <div class="form-control-feedback">
                                <i class="icon-user-check text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group has-feedback has-feedback-left{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" placeholder="@lang('auth.register.your_email')" name="email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                            <div class="form-control-feedback">
                                <i class="icon-mention text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group has-feedback has-feedback-left{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" placeholder="@lang('auth.register.password')" name="password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                            <div class="form-control-feedback">
                                <i class="icon-user-lock text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group has-feedback has-feedback-left">
                            <input type="text" class="form-control" placeholder="@lang('auth.register.repeat_password')" name="password_confirmation">
                            <div class="form-control-feedback">
                                <i class="icon-user-lock text-muted"></i>
                            </div>
                        </div>

                        <button type="submit" class="btn bg-teal btn-block btn-lg">@lang('auth.register.register') <i class="icon-circle-right2 position-right"></i></button>
                        <a href="{{ route('login') }}" class="btn btn-default btn-block">@lang('auth.login.login')</a>
                    </div>
                </form>
                <!-- /advanced login -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->
@endsection
