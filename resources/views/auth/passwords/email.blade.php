@extends('layouts.guest')

@section('content')
    <!-- Page container -->
    <div class="page-container login-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Password recovery -->
                <form action="{{ route('password.email') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="panel panel-body login-form">
                        <div class="text-center">
                            <div class="icon-object border-warning text-warning"><i class="icon-spinner11"></i></div>
                            <h5 class="content-group">
                                @lang('auth.password.email.password_recovery')
                                <small class="display-block">@lang('auth.password.email.email_instructions')</small>
                            </h5>
                        </div>

                        <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" class="form-control" placeholder="@lang('auth.password.email.your_email')" name="email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                            <div class="form-control-feedback">
                                <i class="icon-mail5 text-muted"></i>
                            </div>
                        </div>

                        <button type="submit" class="btn bg-blue btn-block">@lang('auth.password.email.reset_password') <i class="icon-arrow-right14 position-right"></i></button>
                        <a href="{{ route('login') }}" class="btn btn-default btn-block">@lang('auth.login.login')</a>
                    </div>
                </form>
                <!-- /password recovery -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->
@endsection
