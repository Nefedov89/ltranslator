@extends('layouts.guest')

@section('content')
    <!-- Page container -->
    <div class="page-container login-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Advanced login -->
                <form action="{{ route('login') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="panel panel-body login-form">
                        <div class="text-center">
                            <h5 class="content-group-lg">
                                @lang('auth.login.login_to_your_account')
                                <small class="display-block">@lang('auth.login.enter_your_credentials')</small>
                            </h5>
                        </div>

                        <div class="form-group has-feedback has-feedback-left{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" class="form-control" placeholder="@lang('auth.login.email')" name="email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                            <div class="form-control-feedback">
                                <i class="icon-envelope text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group has-feedback has-feedback-left{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" class="form-control" placeholder="@lang('auth.login.password')" name="password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group login-options">
                            <div class="row">
                                <div class="col-sm-6">
                                    <a href="{{ route('password.request') }}">@lang('auth.login.forgot_password')</a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn bg-blue btn-block">
                                @lang('auth.login.login') <i class="icon-arrow-right14 position-right"></i>
                            </button>
                        </div>
                    </div>
                </form>
                <!-- /advanced login -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->
    </div>
    <!-- /page container -->
@endsection
