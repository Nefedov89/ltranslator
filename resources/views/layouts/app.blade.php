<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name') }}</title>

        <!-- styles -->
        @include('layouts.partials.styles')
        <!-- /styles-->

        <!-- scripts -->
        @include('layouts.partials.scripts')
        <!-- /scripts -->
    </head>

    <body>
        <!-- Main navbar -->
        @include('layouts.partials.navbar')
        <!-- /main navbar -->

        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                @include('layouts.partials.sidebar')
                <!-- /main sidebar -->

                <!-- Main content -->
                <div class="content-wrapper">
                    @include('layouts.partials.flash_messages')
                    @yield('content')
                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->
        </div>
        <!-- /page container -->
    </body>
</html>