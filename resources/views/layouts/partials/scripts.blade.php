<script src="{{ asset('assets/js/theme.js') }}"></script>
<script src="{{ asset('assets/js/messages.js') }}"></script>
<script src="{{ asset('assets/js/laroute.js') }}"></script>
<script src="{{ asset('assets/js/app.js') }}"></script>
@yield('page_scripts')