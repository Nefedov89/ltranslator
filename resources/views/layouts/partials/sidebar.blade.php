<div class="sidebar sidebar-main sidebar-default">
    <div class="sidebar-content">

        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-title h6">
                <span>@lang('common.sidebar.menu')</span>
                <ul class="icons-list">
                    <li><a href="#" data-action="collapse"></a></li>
                </ul>
            </div>

            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">
                    <!-- Main -->
                    <li @if(Request::route()->getName() === 'home') class="active" @endif>
                        <a href="{{ route('home') }}">
                            <i class="icon-home4"></i>
                            <span>@lang('common.sidebar.home')</span>
                        </a>
                    </li>
                    <li @if(Request::route()->getName() === 'project.index') class="active" @endif>
                        <a href="{{ route('project.index') }}">
                            <i class=" icon-stack3"></i>
                            <span>@lang('common.sidebar.projects')</span>
                        </a>
                    </li>
                    <li @if(Request::route()->getName() === 'translation.index') class="active" @endif>
                        <a href="{{ route('translation.index') }}">
                            <i class="icon-book3"></i>
                            <span>@lang('common.sidebar.translations')</span>
                        </a>
                    </li>
                    <!-- /main -->
                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
