<?php

return [
    'trans_storage_path'       => resource_path('lang/translations'),
    'google_api_languages_url' => 'https://translation.googleapis.com/language/translate/v2/languages?key=' . env('GOOGLE_API_KEY'),
];
